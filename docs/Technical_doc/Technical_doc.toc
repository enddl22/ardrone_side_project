\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}System Overview}{2}
\contentsline {subsection}{\numberline {2.1}AR.Drone platform}{2}
\contentsline {subsection}{\numberline {2.2}Initial ARDrone coordinates}{2}
\contentsline {subsection}{\numberline {2.3}Navdata}{2}
\contentsline {subsection}{\numberline {2.4}ROS topic name}{3}
\contentsline {section}{\numberline {3}VICON comparison}{4}
\contentsline {section}{\numberline {4}Software building instruction}{5}
\contentsline {subsection}{\numberline {4.1}Building ardrone\_autonomy package}{5}
\contentsline {subsection}{\numberline {4.2}Running ardrone\_autonomy driver}{6}
\contentsline {subsection}{\numberline {4.3}Building ethzasl\_ptam package}{6}
\contentsline {subsection}{\numberline {4.4}Running ethzasl\_ptam package}{7}
\contentsline {subsection}{\numberline {4.5}Building ardrone\_pose\_estimation package}{10}
\contentsline {subsection}{\numberline {4.6}Running ardrone\_pose\_estimation package}{10}
\contentsline {section}{\numberline {5}Kalman Filter for states estimation}{12}
\contentsline {subsection}{\numberline {5.1}States}{12}
\contentsline {subsection}{\numberline {5.2}Process model}{12}
\contentsline {subsection}{\numberline {5.3}Measurement model}{12}
\contentsline {subsection}{\numberline {5.4}Prediction}{13}
\contentsline {subsection}{\numberline {5.5}Update}{13}
\contentsline {section}{\numberline {6}Scale Estimation}{13}
\contentsline {subsection}{\numberline {6.1}Scale calibration}{13}
\contentsline {subsection}{\numberline {6.2}Online scale estimation}{13}
\contentsline {subsection}{\numberline {6.3}Experimental validation}{13}
\contentsline {section}{\numberline {7}Control}{13}
\contentsline {subsection}{\numberline {7.1}System Identification}{13}
\contentsline {subsection}{\numberline {7.2}Controller design}{14}
\contentsline {subsection}{\numberline {7.3}Performance evaluation}{14}
\contentsline {section}{\numberline {8}Experimental results}{14}
