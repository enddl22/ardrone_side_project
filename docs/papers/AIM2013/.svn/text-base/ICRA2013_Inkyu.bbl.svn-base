\begin{thebibliography}{10}

\bibitem{Pounds:fk}
P.~Pounds, R.~Mahony, and P.~Corke, ``Modelling and control of a quad-rotor
  robot,'' in {\em Australasian Conference on Robotics and Automation (ACRA)},
  Dec 2006.

\bibitem{Pounds:2004uq}
P.~Pounds, R.~Mahony, J.~Gresham, P.~Corke, and J.~Roberts, ``Towards
  dynamically-favourable quad-rotor aerial robots,'' in {\em Australasian
  Conference on Robotics and Automation (ACRA)}, Dec 2004.

\bibitem{Bouabdallah:2004mi}
S.~Bouabdallah, A.~Noth, and R.~Siegwart, ``{PID} vs {LQ} control techniques
  applied to an indoor micro quadrotor,'' in {\em Proceedings of IEEE/RSJ
  Conference on Intelligent Robots and Systems (IROS)}, pp.~2451 -- 2456, 2004.

\bibitem{Huang:2009fk}
H.~Huang, G.~Hoffmann, S.~Waslander, and C.~Tomlin, ``Aerodynamics and control
  of autonomous quadrotor helicopters in aggressive maneuvering,'' in {\em
  Proceedings of the IEEE International Conference on Robotics and Automation
  (ICRA)}, pp.~3277 --3282, 2009.

\bibitem{William-Morris:2010vn}
J.~X. William~Morris, Ivan~Dryanovski, ``{3D Indoor Mapping for Micro-UAVs
  Using Hybrid Range Finders and Multi-Volume Occupancy Grids},'' in {\em
  Proceedings of Robotics: Science and Systems (RSS) Workshop on RGB-D
  Cameras}, Jun 2010.

\bibitem{Corke:2011fk}
P.~Corke, {\em {Robotics,Vision and Control Fundamental algorithms in
  MATLAB.}}, vol.~73.
\newblock {Springer}, 2011.

\bibitem{Mahony:2006vn}
R.~Mahony, S.-H. Cha, , and T.~Hamel, ``A coupled estimation and control
  analysis for attitude stabilisation of mini aerial vehicles,'' in {\em
  Australasian Conference on Robotics and Automation (ACRA)}, Dec 2006.

\bibitem{Lupashin:2010fk}
S.~Lupashin, A.~Sch{\"o}andllig, M.~Sherback, and R.~D'Andrea, ``A simple
  learning strategy for high-speed quadrocopter multi-flips,'' in {\em
  Proceedings of the IEEE International Conference on Robotics and Automation
  (ICRA)}, pp.~1642 --1648, May 2010.

\bibitem{Daniel-Mellinger:2010uq}
D.~Mellinger, M.~Shomin, and V.~Kumar, ``Control of quadrotors for robust
  perching and landing,'' in {\em International Powered Lift Conference},
  (Philadelphia, PA), Oct 2010.

\bibitem{Michael:2010fk}
N.~Michael, D.~Mellinger, Q.~Lindsey, and V.~Kumar, ``The {GRASP} {M}ultiple
  {M}icro-{UAV} {T}estbed,'' {\em IEEE Robotics and Automation Magazine},
  vol.~17, pp.~56--65, Sept. 2010.

\bibitem{Bachrach:2010kx}
A.~Bachrach, A.~de~Winter, R.~He, G.~Hemann, S.~Prentice, and N.~Roy, ``{RANGE}
  - robust autonomous navigation in {GPS}-denied environments,'' in {\em
  Proceedings of the IEEE International Conference on Robotics and Automation
  (ICRA)}, pp.~1096 --1097, May 2010.

\bibitem{Sa:2011zr}
``{ROS MikroKopter tutorial and technical documentation}.''
\newblock \url{https://wiki.qut.edu.au/display/cyphy/RosMikroKopter}.

\bibitem{P.Pounds:2009uq}
P.~Pounds, R.~Mahony, and P.~Corke, ``{Modelling and Control of a Large
  Quadrotor Robot},'' in {\em CES}, vol.~18, pp.~691 -- 699, 2009.

\bibitem{Pounds:2009fk}
P.~Pounds and R.~Mahony, ``Design principles of large quadrotors for practical
  applications,'' in {\em Proceedings of the IEEE International Conference on
  Robotics and Automation (ICRA)}, pp.~3265 --3270, May 2009.

\bibitem{Kemp_visualcontrol}
C.~Kemp, {\em {Visual Control of a Miniature Quad-Rotor Helicopter}}.
\newblock PhD thesis, University of Cambridge, 2006.

\bibitem{Mellinger:2011vn}
D.~Mellinger and V.~Kumar, ``{Minimum Snap Trajectory Generation and Control
  for Quadrotors},'' in {\em Proceedings of the IEEE International Conference
  on Robotics and Automation (ICRA)}, May 2011.

\bibitem{Lupashin:2011fk}
S.~Lupashin, A.~Schollig, M.~Hehn, and R.~D'Andrea, ``{The Flying Machine Arena
  as of 2010},'' in {\em Proceedings of the IEEE International Conference on
  Robotics and Automation (ICRA)}, pp.~2970 --2971, May 2011.

\bibitem{He:2008uq}
R.~He, S.~Prentice, and N.~Roy, ``Planning in information space for a quadrotor
  helicopter in a {GPS}-denied environment,'' in {\em Proceedings of the IEEE
  International Conference on Robotics and Automation (ICRA)}, pp.~1814 --1820,
  2008.

\bibitem{Shen:2011kx}
S.~Shen, N.~Michael, and V.~Kumar, ``{Autonomous Multi-Floor Indoor Navigation
  with a Computationally Constrained MAV},'' in {\em Proceedings of the IEEE
  International Conference on Robotics and Automation (ICRA)}, May 2011.

\bibitem{Malis:2002zt}
E.~Malis, J.-J. Borrelly, and P.~Rives, ``Intrinsics-free visual servoing with
  respect to straight lines,'' in {\em Intelligent Robots and Systems, 2002.
  IEEE/RSJ International Conference on}, vol.~1, pp.~384 -- 389 vol.1, 2002.

\bibitem{Abeywardena:2011fk}
D.~Abeywardena, S.~Kodagoda, R.~Munasinghe, and G.~Dissanayake, ``A virtual
  odometer for a quadrotor micro aerial vehicle,'' in {\em ACRA2011}, 2011.

\end{thebibliography}
