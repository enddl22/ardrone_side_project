
#include <ros/ros.h>

#include <cv_bridge/cv_bridge.h>

#include <opencv2/opencv.hpp>

#include <fstream>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/image_encodings.h>

#include <image_transport/image_transport.h>
#include <img_publisher.h>

/*
 * Args: frame_id rate params_file_name camera_file_name start_frame end_frame
 * note that camera_file_name will contain two %d flags, first for the
 * camera number (0 or 1) and second for the frame number
pub_images /camera_0 4 ~/data/stlucia_sub/calibration ~/Downloads/101215_153851_MultiCamera0/cam%d_image%05d.bmp 1000 1500 image:=/image_raw
 */

std::vector<double> matToVect(cv::Mat& mat)
{
  std::vector<double> result;
  result.resize(mat.cols*mat.rows);
  for (int32_t v=0; v<mat.rows; v++)
  {
    for (int32_t u=0; u<mat.cols; u++)
      result[v*mat.cols+u] = mat.at<double>(v, u);
  }
  return result;
}

template <typename T, std::size_t size>
boost::array<T, size> matToArray(cv::Mat& mat)
{
  boost::array<T, size> result;
  for (int32_t v=0; v<mat.rows; v++)
  {
    for (int32_t u=0; u<mat.cols; u++)
      result[v*mat.cols+u] = mat.at<double>(v, u);
  }
  return result;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "pub_images");

  if (argc != 7)
  {
    ROS_ERROR("usage: pub_images frame_id frame_rate params_file image_file_format start_frame end_frame");
    return 1;
  }

  // Get the frame_id
  std::string frame_id(argv[1]);

  // Get the rate
  double frame_rate = (double)atof(argv[2]);
  ROS_INFO("Frame rate: %f", (float)frame_rate);

  // Load image params from file...
  ros::NodeHandle nh;
  std::ifstream file(argv[3]);
  if (file.fail())
  {
    ROS_ERROR("Unable to open file: %s", argv[3]);
    return 1;
  }

  // Image size
  int width, height;
  file >> width >> height;
  cv::Size size(width, height);

  // Camera matrices
  double cm00, cm01, cm02, cm03, cm04, cm05, cm06, cm07, cm08;
  file >> cm00 >> cm01 >> cm02 >> cm03 >> cm04 >> cm05 >> cm06 >> cm07 >> cm08;
  double cm0[] = {cm00, cm01, cm02, cm03, cm04, cm05, cm06, cm07, cm08};
  cv::Mat cm0m = cv::Mat(3,3,CV_64FC1,cm0).clone();

  double cm10, cm11, cm12, cm13, cm14, cm15, cm16, cm17, cm18;
  file >> cm10 >> cm11 >> cm12 >> cm13 >> cm14 >> cm15 >> cm16 >> cm17 >> cm18;
  double cm1[] = {cm10, cm11, cm12, cm13, cm14, cm15, cm16, cm17, cm18};
  cv::Mat cm1m = cv::Mat(3,3,CV_64FC1,cm1).clone();

  // Distortion matrices
  double cd00, cd01, cd02, cd03, cd04;
  file >> cd00 >> cd01 >> cd02 >> cd03 >> cd04;
  double cd0[] = {cd00, cd01, cd02, cd03, cd04};
  cv::Mat cd0m = cv::Mat(1,5,CV_64FC1,cd0).clone();

  double cd10, cd11, cd12, cd13, cd14;
  file >> cd10 >> cd11 >> cd12 >> cd13 >> cd14;
  double cd1[] = {cd10, cd11, cd12, cd13, cd14};
  cv::Mat cd1m = cv::Mat(1,5,CV_64FC1,cd1).clone();

  // Rotation
  double r0, r1, r2;
  file >> r0 >> r1 >> r2;
  double r[] = {r0, r1, r2};
  cv::Mat rm = cv::Mat(3,1,CV_64FC1,r).clone();

  // Translation
  double t0, t1, t2;
  file >> t0 >> t1 >> t2;
  double t[] = {t0, t1, t2};
  cv::Mat tm = cv::Mat(3,1,CV_64FC1,t).clone();

  file.close();

  // Rectification
  cv::Mat cr0m(3,3,CV_64FC1);
  cv::Mat cr1m(3,3,CV_64FC1);

  // Projection
  cv::Mat cp0m(3,4,CV_64FC1);
  cv::Mat cp1m(3,4,CV_64FC1);

  // Q matrix?
  cv::Mat qm(4,4,CV_64FC1);

  // Fill the missing params
  cv::stereoRectify(cm0m, cd0m, cm1m, cd1m, size, rm, tm, cr0m, cr1m, cp0m, cp1m, qm);

  ROS_INFO_STREAM(         "Loaded the following parameters:\n"
                        << "Camera 0"
                        << "\n--Camera matrix:\n" << cm0m 
                        << "\n--Distortion coefficients:\n" << cd0m
                        << "\n--Rectification matrix:\n" << cr0m
                        << "\n--Projection matrix:\n" << cp0m << "\n"
                        << "Camera 1"
                        << "\n--Camera matrix:\n" << cm1m
                        << "\n--Distortion coefficients:\n" << cd1m
                        << "\n--Rectification matrix:\n" << cr1m
                        << "\n--Projection matrix:\n" << cp1m << "\n"
                        << "Rotation: " << rm << "\n"
                        << "Translation: " << tm << "\n");

  // Now create the left/right camera info messages
  sensor_msgs::CameraInfo left_info, right_info;

  left_info.header.frame_id = frame_id;
  left_info.width = width;
  left_info.height = height;
  left_info.distortion_model = "plumb_bob";
  left_info.D = matToVect(cd0m);
  left_info.K = matToArray<double, 9>(cm0m);
  left_info.R = matToArray<double, 9>(cr0m);
  left_info.P = matToArray<double, 12>(cp0m);
  left_info.binning_x = 0;
  left_info.binning_y = 0;
  left_info.roi.x_offset = 0;
  left_info.roi.y_offset = 0;
  left_info.roi.height = 0;
  left_info.roi.width = 0;
  left_info.roi.do_rectify = false;

  right_info.header.frame_id = frame_id;
  right_info.width = width;
  right_info.height = height;
  right_info.distortion_model = "plumb_bob";
  right_info.D = matToVect(cd1m);
  right_info.K = matToArray<double, 9>(cm1m);
  right_info.R = matToArray<double, 9>(cr1m);
  right_info.P = matToArray<double, 12>(cp1m);
  right_info.binning_x = 0;
  right_info.binning_y = 0;
  right_info.roi.x_offset = 0;
  right_info.roi.y_offset = 0;
  right_info.roi.height = 0;
  right_info.roi.width = 0;
  right_info.roi.do_rectify = false;

  // Create the info publishers
  std::string stereo_ns = nh.resolveName("stereo");
  std::string left_info_topic = stereo_ns + "/left/camera_info";
  std::string right_info_topic = stereo_ns + "/right/camera_info";

  ros::Publisher left_info_pub = nh.advertise<sensor_msgs::CameraInfo>(left_info_topic, 1);
  ros::Publisher right_info_pub = nh.advertise<sensor_msgs::CameraInfo>(right_info_topic, 1);

  // Create the image publishers
  image_transport::ImageTransport it(nh);
  std::string left_topic = ros::names::clean(stereo_ns + "/left/" + nh.resolveName("image"));
  std::string right_topic = ros::names::clean(stereo_ns + "/right/" + nh.resolveName("image"));
  image_transport::Publisher left_pub = it.advertise(left_topic, 1);
  image_transport::Publisher right_pub = it.advertise(right_topic, 1);

  // Grab the camera file name and initial frame
  char* camera_file_name = argv[4];
  uint32_t start_frame = atoi(argv[5]);
  uint32_t end_frame = atoi(argv[6]);

  uint32_t frame = start_frame;

  // The main loop
  ros::Rate loop_rate(frame_rate);
  while (ros::ok())
  {
    // Update
    ros::Time now = ros::Time::now();
    left_info.header.stamp = now;
    right_info.header.stamp = now;

    // Get the left and right image locations
    char left_image_str[1000];
    sprintf(left_image_str, camera_file_name, 0, frame);
    char right_image_str[1000];
    sprintf(right_image_str, camera_file_name, 1, frame);

    // Load the images. Note that ocv stores them by default as BGR, but ROS expect RGB
    // Note to self: make the cvtColor call args dependent on some command line argument.
    // This way it can be used with whatever and stuff
    cv_bridge::CvImage left_image;
    left_image.image = cv::imread(left_image_str, 0);
    cv::cvtColor(left_image.image, left_image.image, CV_BayerGR2RGB); // CV_BGR2RGB
    left_image.header = left_info.header;
    size_t l_size = left_image.image.elemSize1();
    if (l_size == 1) left_image.encoding = sensor_msgs::image_encodings::RGB8;
    if (l_size == 2) left_image.encoding = sensor_msgs::image_encodings::RGB16;

    cv_bridge::CvImage right_image;
    right_image.image = cv::imread(right_image_str, 0);
    cv::cvtColor(right_image.image, right_image.image, CV_BayerGR2RGB); // CV_BGR2RGB
    right_image.header = right_info.header;
    size_t r_size = right_image.image.elemSize1();
    if (r_size == 1) right_image.encoding = sensor_msgs::image_encodings::RGB8;
    if (r_size == 2) right_image.encoding = sensor_msgs::image_encodings::RGB16;

    // Publish
    left_info_pub.publish(left_info);
    right_info_pub.publish(right_info);
    left_pub.publish(left_image.toImageMsg());
    right_pub.publish(right_image.toImageMsg());

    frame++;
    if (frame > end_frame) break;

    // Get out of here
    ros::spinOnce();
    loop_rate.sleep();
  }

  ROS_INFO("Playback completed");
  return 0;
}
