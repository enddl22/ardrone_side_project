/* $Author: Luis Mejias$
 * 
 *  This program can be used to connect to the vicon system and visualise its
 *  output, position and  orientation at this point. For a more details 
 *  about the type of information the vicon system can provide please see the 
 *  API/SDK on the ARCAA wiki.
 *
 */

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>

#include <sys/time.h>
struct timeval tv;

/*
// system OpenGL includes
#include <GL/gl.h> 
#include <GL/glu.h>
#include <GL/glut.h>
*/

//Vicon SDK header
#include "Client.h"




//define the view for the  openGL camera
typedef enum {
	view_stationary,
	view_walk_behind,
	view_fly_behind,
	view_cockpit,
	view_north_up,
	view_track_up,
	heli_cam,
	view_exploring_right,
	view_exploring_left,
	view_exploring_up
} viewpoint_t;



//main method to display/visualisation
void DrawScene( viewpoint_t		viewpoint,
		double			north,
		double			east,
		double			down,
		double			phi,
		double			theta,
		double			psi,
		double			roll_moment,
		double			pitch_moment );

//traslation/orientation of the openGL camera camera/view 
static void lookat( viewpoint_t		viewpoint,
		    double			x,
		    double			y,
		    double			z,
		    double			phi,
		    double			theta,
		    double			psi
		    );
//draw a small red cube on the display 
void drawBox(void);
//draw text in the main window
void drawString (int x,int y, int z,char *s);


//openGL definitions
static int		main_window;
static int		WIDTH		= 600; //width of the openGL window
static int		HEIGHT		= 300;//height of the openGL window
viewpoint_t	viewpoint   = view_stationary;//view of the openGL camera
#define C_DEG2RAD           0.0175            // degree to radian
#define C_RAD2DEG           57.2958           // radian to degree
#define mm2M                0.001  

//main position and orientation of the vicon object
float X,Y,Z;   //position
float phi,theta,psi; //orientation

//vicon data stream namespace
using namespace ViconDataStreamSDK::CPP;

// Make a new client
Client MyClient;

/**
 * reads incoming data stream from the vicon server. is called in the main 
 * visualisation loop 
 */
static void process_state(void){
  
  std::string SegmentName;
  std::string SubjectName;
  unsigned int SubjectCount;
  unsigned int SegmentCount;


  while( MyClient.GetFrame().Result != Result::Success )
    {
      // Sleep a little so that we don't lumber the CPU with a busy poll
      sleep(1);
      std::cout << ".";
    }
  
  //number of objects currently represented/tracked by the vicon system 
  SubjectCount = MyClient.GetSubjectCount().SubjectCount;
  
  
  unsigned int SubjectIndex = 0 ; //we just plot the first object in the list
                                  //incrementing until subjectCount will display
                                  //several object simultaneously


  SubjectName = MyClient.GetSubjectName( SubjectIndex ).SubjectName;
      
  // Get the root segment
  std::string RootSegment = MyClient.GetSubjectRootSegmentName( SubjectName ).SegmentName;
  
  //Get the number of Segments
  SegmentCount = MyClient.GetSegmentCount( SubjectName ).SegmentCount;
      

  
  unsigned int SegmentIndex = 0; //we just plot the first object in the list
                                  //incrementing until segmentCount will display
                                  //several object simultaneously

   //Get the name of Segment
  SegmentName = MyClient.GetSegmentName( SubjectName, SegmentIndex ).SegmentName;
  //Get the name of subject
  SubjectName = MyClient.GetSubjectName( SubjectIndex ).SubjectName;


  // Get the global segment translation
  Output_GetSegmentGlobalTranslation _Output_GetSegmentGlobalTranslation = MyClient.GetSegmentGlobalTranslation( SubjectName, SegmentName );
      
  //store locally X,Y,Z. data comes by defauls in mm and radians
  X = _Output_GetSegmentGlobalTranslation.Translation[ 0 ]*mm2M;
  Y = _Output_GetSegmentGlobalTranslation.Translation[ 1 ]*mm2M;
  Z = -1.0 * (_Output_GetSegmentGlobalTranslation.Translation[ 2 ])*mm2M;
  
  // Get the global segment orientation
  Output_GetSegmentGlobalRotationEulerXYZ _Output_GetSegmentGlobalRotationEulerXYZ = MyClient.GetSegmentGlobalRotationEulerXYZ( SubjectName, SegmentName );
  
  //store locally roll, pitch, yaw
  phi= _Output_GetSegmentGlobalRotationEulerXYZ.Rotation[ 0 ];
  theta= _Output_GetSegmentGlobalRotationEulerXYZ.Rotation[ 1 ];
  psi=  _Output_GetSegmentGlobalRotationEulerXYZ.Rotation[ 2 ];
  
  //print in console current position and attitude
  //std::cout << "Global Translation: (" << X << ", " << Y << ", " << Z << ") " << std::endl;
  //std::cout << "Global Orientation(" << phi << ", " << theta << ", " << psi << ") " << std::endl;
  gettimeofday(&tv,NULL);
//  std::cout<<"sec"<<tv.tv_sec<<std::endl; // seconds
//  std::cout<<"usec"<<tv.tv_usec<<std::endl; // microseconds

  std::cout << tv.tv_sec<<"."<<tv.tv_usec<<", "<<X << ", "<< Y << ", "<< Z  << ", "<< phi << ", " << theta << ", " << psi << std::endl;
  
  
  
}

/**
 *  rezise_window() resize main window
 *  
 */


/*
static void resize_window( GLsizei  w,
			   GLsizei  h
			   )
{
  if( h == 0 )
    h = 1;
  if( w == 0 )
    w = 1;
  
  glViewport( 0, 0, w, h );
  glMatrixMode( GL_PROJECTION );
  glLoadIdentity();
  gluPerspective(
		 30.0,				// Field of view
		 (GLfloat)w/(GLfloat)h,		// Aspect ratio
		 1.0,				// Near
		 1000.0				// Far
		 );
  
  // select the Modelview matrix
  glMatrixMode( GL_MODELVIEW );
  glLoadIdentity();
}
*/


/**
 *  animate() main CALLBACK visualisation function
 */




/**
 *  init_window() sets up the GL environment and schedules the
 * user callbacks for animation and periodic timing.
 */

/*
void
init_window(
	    int *   argc,
	    char ** argv,
	    void    (*animate)( void ),
	    void    (*keyboard)(
				unsigned char  c,
				int	       x,
				int	       y
				),
	    void    (*idle)( void )
	    )
{
  // Call these first so that glutInit can override if necessary
  glutInit( argc, argv );
  glutInitWindowSize( WIDTH, HEIGHT ); 
  glutInitWindowPosition( 320, 100 );
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH );	
  main_window = glutCreateWindow( argv[0] );
  
  glShadeModel(GL_FLAT);
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glClearDepth(1.0f);
  glEnable( GL_DEPTH_TEST );
  glDepthFunc( GL_LEQUAL );
  glEnable( GL_BLEND );
  glEnable( GL_ALPHA_TEST );
  
  //callbacks for display, keyboard and resize window
  glutDisplayFunc( animate );
  glutReshapeFunc( resize_window ); 
  glutKeyboardFunc( keyboard );
  //glutIdleFunc( idle ); //not used at the moment. could be used to call process_state
  
  
}
*/

/**
 * keyboard() read the keyboards and set the view
 *  of the display/window.
 */

static void
keyboard(
	 unsigned char		c,
	 int			x,
	 int			y
	 )
{
  
  
  switch( c )
    {
    case ' ':
      printf( "reset\n" );
      break;
    case 'q':
      printf( "quit\n" );
      // Disconnect and dispose
      MyClient.Disconnect();
      exit( EXIT_SUCCESS );
    case '1':
      viewpoint = view_stationary;
      break;
    case '2':
      viewpoint = view_walk_behind;
      break;
    case '3':
      viewpoint = view_fly_behind;
      break;
    case '4':
      viewpoint = heli_cam;
      break;
    case '5':
      viewpoint = view_north_up;
      break;
    case '6':
      viewpoint = view_track_up;
      break;
    case '7':
      viewpoint =  view_exploring_left;
      break;
    case 'v':   
      break;
    default:
      /* Nothing */
      break;
    }
  
	
	fflush( stdout );
}


/**
 * mai() main function
 *  
 */

int main(int argc, char **argv )
{
  
  bool TransmitMulticast        = false;
  std::string HostName = "131.181.87.177:801";

  
  while( !MyClient.IsConnected().Connected )
    {
      // Direct connection
      MyClient.Connect( HostName );
      std::cout << ".";
      sleep(1);
      
    }
  
  // Enable some different data types
  MyClient.EnableSegmentData();
  MyClient.EnableMarkerData();
  MyClient.EnableUnlabeledMarkerData();
  MyClient.EnableDeviceData();
  
  // Set the streaming mode
  MyClient.SetStreamMode( ViconDataStreamSDK::CPP::StreamMode::ClientPull );
  
  
  // Set the global up axis
  MyClient.SetAxisMapping( Direction::Forward, 
                           Direction::Left, 
                           Direction::Up ); // Z-up
  
  
  /*
  //initialise the entire program
  init_window(	&argc,
		argv,
		animate,
		keyboard,
		process_state);
*/
	std::cout << "time,x,y,z,phi,theta,yaw"<< std::endl;
	while(1)
	{
		process_state();
		sleep(0.001);
	};


	// Process remaining arguments
	fprintf( stderr, "%d args remaining\n", argc );
	while( *++argv )
	{
		const char *		arg = *argv;
		if( arg[0] != '-' )
			break;

		if( isdigit( arg[1] ) )
		{
			viewpoint = (viewpoint_t) (arg[1] - '1');
			continue;
		}

		fprintf( stderr,
			"heli-3d: Unknown argument '%s'\n",
			arg
		);
			
	}


	//glutMainLoop();
}

