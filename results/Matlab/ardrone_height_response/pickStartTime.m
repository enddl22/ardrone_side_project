function out=pickStartTime(time)
    if time.goal(1) > time.nav(1)
        out = time.nav(1);
    else
        out = time.goal(1);
    end
end