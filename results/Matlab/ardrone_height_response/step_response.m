goal=importdata('goal.txt');
nav=importdata('navdata.txt');

time = struct('goal',goal(:,1)/1e9,'nav',nav(:,1)/1e9);

startTime=pickStartTime(time);
time.goal = time.goal-startTime;
time.nav = time.nav-startTime;

h_goal = goal(:,5);
h_measure = nav(:,8);

figure;
% This goal should be fixed....
%plot(time.goal,h_goal,'r*');
%hold on;
stairs(time.goal,h_goal,'r');
hold on;
plot(time.nav,h_measure,'b');
legend('Goal','Measure');
xlabel('Sec');
ylabel('Metre');
grid on;
