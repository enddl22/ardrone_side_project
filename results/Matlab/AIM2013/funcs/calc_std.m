set(0,'CurrentFigure',fig);
disp('Select range');
fp = ginput(2);
start_t = fp(1,1);
end_t = fp(2,1);
b_idx=min(find(abs(time - start_t)< 0.2));
e_idx=min(find(abs(time - end_t)< 0.2));

std_t_new=std(new_t(b_idx:e_idx,:))
std_R_new=rad2deg(std(new_R(b_idx:e_idx,:)))

fprintf('Time period = %fs ~ %fs\n',time(b_idx),time(e_idx));
fprintf('std x =%f(m)\n',std_t_new(1));
fprintf('std y =%f(m)\n',std_t_new(2));
fprintf('std z =%f(m)\n',std_t_new(3));
fprintf('std roll =%f(deg)\n',std_R_new(1));
fprintf('std pitch =%f(deg)\n',std_R_new(2));
fprintf('std yaw =%f(deg)\n',std_R_new(3));
fprintf('\n\n');