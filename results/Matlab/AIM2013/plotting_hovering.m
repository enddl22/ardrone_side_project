close all
clear all

path(path,'./funs');

pose_new = importdata('./indoor_lab_hovering05/indoor_new_pose.txt',',',1);
pose = importdata('./indoor_lab_hovering05/indoor_pose.txt',',',1);
time_new = pose_new.data(:,1)/1e9;
time_new = time_new-time_new(1);
time = pose.data(:,1)/1e9;
time = time-time(1);
new_t = pose_new.data(:,5:7);
new_R = pose_new.data(:,8:10);
t = pose.data(:,5:7);
R = pose.data(:,8:10);
ts = mean(diff(time));
check_length;

comp_yaw;
plot_t;
plot_R;
calc_std;
plot_3D_hover_new;
plot_3D_hover;





