clear all;
close all;
cmd_data = importdata('./cmd_yaw01.txt',',',1);
nav_data = importdata('./nav_yaw01.txt',',',1);

time=struct('cmd',cmd_data.data(:,1)/1e9,'navdata',nav_data.data(:,1)/1e9);

yaw=nav_data.data(:,8); %deg
yaw_dot=nav_data.data(:,18);
yaw = yaw-yaw(1); % remove offset


cmd_intp = interp1(time.cmd,cmd_data.data(:,10),time.navdata,'nearest');

%vz = nav_data.data(:,12);
%accz = nav_data.data(:,15);






start_time = time.navdata(1);

time.cmd = time.cmd-start_time;
time.navdata = time.navdata-start_time;
%plot(time.cmd,cmd,'b');

u=cmd_intp;
y=yaw;
ts = mean(diff(time.navdata));

%{
%{
%stairs(time.cmd,cmd_data.data(:,10));
[time.cmd,cmd]=stairs(time.cmd,cmd_data.data(:,10));
time.cmd=time.cmd+randn(numrows(time.cmd),1)*0.0001;
cmd=cmd+randn(numrows(cmd),1)*0.0001;

cmd_intp = interp1(time.cmd,cmd,time.navdata,'spline');
k=find(isnan(cmd_intp));
cmd_intp(k)=0;
figure;
plot(time.navdata(5460:10684),cmd_intp,'r');
%}



%{
figure;
plot(time.cmd,cmd,'b');
hold on;
plot(time.navdata,height,'r');
legend('cmd','height');
%}
%time.cmd(diff(time.cmd)<0)=0;





figure;
%plot(time.cmd,cmd,'b*');
%stairs(time.cmd,-cmd,'b');
plot(time.cmd,-cmd,'b');
hold on;
plot(time.navdata,yaw_dot*10,'r');
legend('cmd','yaw dot');

figure;
%plot(time.cmd,100*cmd,'b*');
%stairs(time.cmd,100*cmd,'b');
plot(time.cmd,100*cmd,'b');
hold on;
plot(time.navdata,yaw,'r');
legend('cmd*100','yaw');

%color = rand(6,3);
figure;
%{
plot(vz,'b');
hold on;
plot(accz,'r');
hold on;
%}
plot(yaw,'k');
hold on;
plot(cmd_intp,'--b');
legend('vz','accz','yaw','cmd');
u = cmd_intp;
y = yaw;
ts = mean(diff(time.navdata));

%{
input = cmd_intp(9900:12000,1);
output = yaw(9900:12000,1);

output=output - output(1);
[max_ip,max_idx]=max(input);
[min_ip,min_idx]=min(input);

time_range=0:0.005:((12000-9900)*0.005);


temp_ip=zeros(size(input));
temp_ip=sign([0;diff(input)]);

cross_negpos = find((diff(sign(temp_ip))== 2) | (diff(sign(temp_ip))== 1));
cross_negpos=cross_negpos(2:end);

cross_posneg = find((diff(sign(temp_ip))== -2) | (diff(sign(temp_ip))== -1));

temp_ip(1:(cross_posneg(1)-1))=0;

for i=1:(length(cross_negpos)-1)
temp_ip(cross_posneg(i):(cross_negpos(i)-1))=0.5;
temp_ip(cross_negpos(i):(cross_posneg(i+1)-1))=-0.5;
end

temp_ip(cross_negpos(4):end)=0;


figure(3)
plot(time_range,input,time_range,output,time_range,temp_ip)
input=temp_ip;
figure(4)
plot(time_range,input,time_range,output)

%}
%}
