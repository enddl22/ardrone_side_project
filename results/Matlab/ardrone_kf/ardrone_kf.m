% Copyright (C) 2012, by Inkyu (i.sa@qut.edu.au)
%
% This file is part of The ardrone_navi project
% Kalman filter estimation
% 
% States= {x,y,vx,vy}=X
% Measurements = {vx,vy}=V
% Process model:
% X(k+1) = X(k)+R*V;
%
% Measurement model:
% Z=H*X;
% This measurements are obtained from a AR.Drone that fuses optical flow
% and IMU.
% 
% This is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% RTB is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Leser General Public License
% along with RTB.  If not, see <http://www.gnu.org/licenses/>.

clear all;
close all;
pose = importdata('./pose.txt',',',1);
nav = importdata('./navdata.txt',',',1);

%<<<<<<< HEAD
%=======

%>>>>>>> c1f4b4767a1acc4c7a2e401b801f9e98565af9e0
pos = struct('x',pose.data(:,5),'y',pose.data(:,6),'z',pose.data(:,7),'yaw',pose.data(:,10));
ori = struct('roll',pose.data(:,8),'pitch',pose.data(:,9),'yaw',pose.data(:,10));
pos.x = [pos.x;pos.x(end)];
pos.y = [pos.y;pos.y(end)];
pos.z = [pos.z;pos.z(end)];
pos.yaw = [pos.yaw;pos.yaw(end)];


ori.roll = [ori.roll;ori.roll(end)];
ori.pitch = [ori.pitch;ori.pitch(end)];
ori.yaw = [ori.yaw;ori.yaw(end)];


vel = struct('x',nav.data(:,10),'y',nav.data(:,11));

time = struct('pose',pose.data(:,1),'nav',nav.data(:,1));
time.nav = (time.nav-time.nav(1))/1.0e+09;

ts=1/200; %200Hz

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Interpolation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


time_interp=[0:ts:60]'; 


pos_intp=struct('x',interp1(time.nav,pos.x,time_interp,'cubic'),...
                'y',interp1(time.nav,pos.y,time_interp,'cubic'),...
                'z',interp1(time.nav,pos.z,time_interp,'cubic'));
vel_intp=struct('x',interp1(time.nav,vel.x,time_interp,'cubic'),...
                'y',interp1(time.nav,vel.y,time_interp,'cubic'));
ang_intp=struct('roll',interp1(time.nav,ori.roll,time_interp,'cubic'),...
                'pitch',interp1(time.nav,ori.pitch,time_interp,'cubic'),...
                'yaw',interp1(time.nav,pos.yaw,time_interp,'cubic'));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Process noise,Q
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
velnoise = 0.003; % velocity process noise (m/s)
posnoise = 0.003; % position process noise (m)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Measurement noise,R
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
mea_velnoise = 1.5; % velocity measurement noise (m)

H = [0 0 1 0;0 0 0 1]; % measurement matrix
x = [pos_intp.x(1); pos_intp.y(1);vel_intp.x(1);vel_intp.y(1)]; % initial state vector
x_hat = x; % initial state estimate
Q = diag([posnoise,posnoise,velnoise,velnoise].^2);
R = diag([mea_velnoise mea_velnoise].^2);

p=[20 20 20 20];
P = diag(p,0);

% set up the size of the innovations vector
Inn = zeros(size(R));


result_xhat=x;
result_P=P;
x_st = x_hat;
P_st = P;

%begin=1901;
%finish=2773;

begin=1;
finish=numel(ang_intp.yaw);

for t = begin : finish
    
    % transition matrix
    F = [1 0 ts*cos(ang_intp.yaw(t)) -sin(ang_intp.yaw(t))*ts;...
         0 1 ts*sin(ang_intp.yaw(t)) ts*cos(ang_intp.yaw(t));...
         0 0 1 0;0 0 0 1]; 

    x_hat= F*x_hat;
    % Covariance prediction
    P=F*P*F'+Q;
   
    % Update Step
    %z = [pos_intp.x(t);pos_intp.y(t)];
    z = [vel_intp.x(t);vel_intp.y(t)];
    
    v_inn= z-H*x_hat;
    s=H*P*H'+R;
    w=P*H'*inv(s);
    
    x_hat=x_hat+w*v_inn;
    P=P-w*s*w';
    %x_hat = x_hat';
    
    x_st(:,t)=x_hat;
    P_st=[P_st;P];
    
    
end

x_new = x_st(1,1);
y_new = x_st(2,1);
x_esti=x_new;
y_esti=y_new;

for i=begin:finish
    x_new = x_new+ts*( cos(ang_intp.yaw(i))*x_st(3,i)-sin(ang_intp.yaw(i))*x_st(4,i));
    y_new = y_new+ts*( sin(ang_intp.yaw(i))*x_st(3,i)+cos(ang_intp.yaw(i))*x_st(4,i));
    x_esti = [x_esti;x_new];
    y_esti = [y_esti;y_new];
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Position plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure();

hold on;
plot(time_interp(begin:finish),  pos_intp.x(begin:finish),'b');



plot(time_interp(begin:finish),x_st(1,(begin:finish)),'r');


%plot(time_interp(begin:finish),x_esti(1:end-1),'g');

hold off;


set(gca,'fontsize',16);
xlabel('sec', 'fontsize',16);
ylabel('m', 'fontsize',16);
h_legend=legend('mea x','kf x','with kf vx',16);
set(h_legend,'fontsize',16);
grid;

figure();

hold on;
plot(pos_intp.x(begin:finish),pos_intp.y(begin:finish),'bo');

plot(x_st(1,(begin:finish)),x_st(2,(begin:finish)),'rd');

%plot(x_esti(begin:finish),y_esti(begin:finish),'g*');


hold off;


set(gca,'fontsize',16);
xlabel('sec', 'fontsize',16);
ylabel('m', 'fontsize',16);
h_legend=legend('Measured','Estimated',16);
set(h_legend,'fontsize',16);
grid;




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Velocity plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure();
plot(time_interp(begin:finish),  vel_intp.x(begin:finish),'b');
hold on;
plot(time_interp(begin:finish),x_st(3,(begin:finish)),'r');
hold off;
set(gca,'fontsize',16);
xlabel('sec', 'fontsize',16);
ylabel('m/s', 'fontsize',16);
h_legend=legend('mea vx','kf vx',16);
set(h_legend,'fontsize',16);
grid;



%{
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Covariance plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% x covariance
figure();
plot(time_interp(begin:finish),3*sqrt(P_st(1:4:end-4,1)),'k');
hold on;
plot(time_interp(begin:finish),-3*sqrt(P_st(1:4:end-4,1)),'k');
hold on;


set(gca,'fontsize',16);
xlabel('sec', 'fontsize',16);
ylabel('m', 'fontsize',16);
h_legend=legend('covariance x',16);
set(h_legend,'fontsize',16);
grid;


figure();
plot(time_interp(begin:finish),3*sqrt(P_st(3:4:end-4,3)),'k');
hold on;
plot(time_interp(begin:finish),-3*sqrt(P_st(3:4:end-4,3)),'k');
hold on;


set(gca,'fontsize',16);
xlabel('sec', 'fontsize',16);
ylabel('m', 'fontsize',16);
h_legend=legend('covariance vx',16);
set(h_legend,'fontsize',16);
grid;
%}


