% Scale estimation simulation
%
%   x, y: two n-dimentional data sets
%   x ~ lambda*y
%   this file is to estimate lambda based on x and y

% Configuration
lambda1_star_set = [];
lambda2_star_set = [];
lambda3_star_set = [];
arith_mean_set = [];
geo_mean_set = [];
median_set = [];
true_lambda = 2;
NumOfSample = 10000;

% Generate two data following guassian distribustion
y_mean = [10 15 25];
x_mean = 2 * y_mean;
y_sigma = 1.0;
x_sigma = 0.3;

% variables are independent and with homogeneous variance
y_SIGMA = y_sigma*eye(3);
x_SIGMA = x_sigma*eye(3);

Y = mvnrnd(y_mean, y_SIGMA, NumOfSample);
X = mvnrnd(x_mean, x_SIGMA, NumOfSample);


% ***************************************************
% Scale estimation
tic
for k = 1:NumOfSample
    x = X(1:k, :);
    y = Y(1:k, :);
    xTy = sum(sum(y.*x, 2));
    xTx = sum(sum(x.*x, 2));
    yTy = sum(sum(y.*y, 2));
    
    % Method 1
    lambda1_star = xTy/yTy;
    lambda1_star_set = [lambda1_star_set lambda1_star];
    
    % Method 2
    lambda2_star = xTx/xTy;
    lambda2_star_set = [lambda2_star_set lambda2_star];
    
    % Method 3
    Sxx = y_sigma^2*xTx;
    Syy = x_sigma^2*yTy;
    Sxy = y_sigma*x_sigma*xTy;
    lambda3_star = x_sigma*(Sxx - Syy + sign(Sxy)*sqrt((Sxx - Syy)^2 + 4*Sxy^2))/(2*y_sigma*Sxy);
    lambda3_star_set = [lambda3_star_set lambda3_star];
    
    %---------------------
    % Other estimator
    %---------------------
    x_norm = sqrt(sum(x.*x, 2));
    y_norm = sqrt(sum(y.*y, 2));
    x_y_norm = x_norm./y_norm;
    % ******** arithmetic mean *********
    arith_mean = mean(x_y_norm);
    arith_mean_set = [arith_mean_set arith_mean];
    
    % ******** geometric mean **********
    geo_mean = geomean(x_y_norm);
    geo_mean_set = [geo_mean_set geo_mean];
    
    % ******** median ******************
    median_val = median(x_y_norm);
    median_set = [median_set median_val];
end
toc

figure()
semilogx(lambda3_star_set, 'color', [153, 0, 51]/255, 'LineWidth', 2)
hold on
semilogx(lambda2_star_set, 'k--', 'LineWidth', 1.2)
semilogx(lambda1_star_set, 'r--', 'LineWidth', 1.2)
semilogx(arith_mean_set, 'b-', 'LineWidth', 1.2)
semilogx(geo_mean_set, 'k-', 'LineWidth', 1.2)
semilogx(median_set, 'g-', 'LineWidth', 1.2)
semilogx(true_lambda*ones(size(lambda1_star_set)), 'k-')
ylim([1.5 3])
xlabel('number of samples')
ylabel('estimated scale')
legend('\lambda^{\ast}','\lambda_{x}^{\ast}','\lambda_{y}^{\ast}','arith.mean','geo.mean','median','Orientation','horizontal')

