% Scale estimation simulation
%
%   x, y: two n-dimentional data sets
%   x ~ lambda*y
%   this file is to estimate lambda based on x and y

sonar = importdata('./ptam_n_sonar_z/sonar_n_ptam_navdata.txt',',',1);
ptam = importdata('./ptam_n_sonar_z/sonar_n_ptam_PTAM_Z.txt',',',1);

% Configuration
lambda1_star_set = [];
lambda2_star_set = [];
lambda3_star_set = [];
arith_mean_set = [];
geo_mean_set = [];
median_set = [];


y_sigma = 0.0011;
x_sigma = 0.0016;

sonar_z = sonar.data(:, 14);
ptam_z = ptam.data(:, 3);

interval = (length(ptam_z) - 1)/length(sonar_z);
new_ptam_z = interp1(1:length(ptam_z), ptam_z, 1:interval:length(ptam_z), 'spline')';

if length(new_ptam_z) > length(sonar_z)
    new_ptam_z(end) = [];
end

if length(new_ptam_z) < length(sonar_z)
    sonar_z(end) = [];
end

% sonar data
Y = sonar_z(1:end-800);
% PTAM data
X = new_ptam_z(1:end-800);


NumOfSample = length(X);

% % Generate two data following guassian distribustion
% y_mean = 10;
% x_mean = 2 * y_mean;
% 
% 
% % Y = mvnrnd(y_mean, y_sigma, NumOfSample);
% % X = mvnrnd(x_mean, x_sigma, NumOfSample);
% 
% Y = y_mean + y_sigma*randn(NumOfSample,1);
% X = x_mean + x_sigma*randn(NumOfSample,1);

% ***************************************************
% Scale estimation
tic
for k = 1:NumOfSample
    x = X(1:k, :);
    y = Y(1:k, :);
    xTy = sum(sum(y.*x, 2));
    xTx = sum(sum(x.*x, 2));
    yTy = sum(sum(y.*y, 2));
    
    % Method 1
    lambda1_star = xTy/yTy;
    lambda1_star_set = [lambda1_star_set lambda1_star];
    
    % Method 2
    lambda2_star = xTx/xTy;
    lambda2_star_set = [lambda2_star_set lambda2_star];
    
    % Method 3
    Sxx = y_sigma^2*xTx;
    Syy = x_sigma^2*yTy;
    Sxy = y_sigma*x_sigma*xTy;
    lambda3_star = x_sigma*(Sxx - Syy + sign(Sxy)*sqrt((Sxx - Syy)^2 + 4*Sxy^2))/(2*y_sigma*Sxy);
    lambda3_star_set = [lambda3_star_set lambda3_star];
    
    %---------------------
    % Other estimator
    %---------------------
    x_norm = sqrt(sum(x.*x, 2));
    y_norm = sqrt(sum(y.*y, 2));
    x_y_norm = x_norm./y_norm;
    % ******** arithmetic mean *********
    arith_mean = mean(x_y_norm);
    arith_mean_set = [arith_mean_set arith_mean];
    
    % ******** geometric mean **********
    geo_mean = geomean(x_y_norm);
    geo_mean_set = [geo_mean_set geo_mean];
    
    % ******** median ******************
    median_val = median(x_y_norm);
    median_set = [median_set median_val];
end
toc

scaled_ptam_z = X/lambda3_star;


figure()
subplot(2,2,1)
plot(Y)
ylabel('Sonar meaurements')
ylim([0.5 1.5])

subplot(2,2,2)
plot(X)
ylabel('PTAM z')
ylim([0.5 1.5])

subplot(2,2,3)
plot(Y, 'k', 'LineWidth', 1.2)
hold on
plot(scaled_ptam_z, 'r--')
legend('Sonar', 'Scaled PTAM')
ylim([0.5 1.5])

subplot(2,2,4)
plot(Y - scaled_ptam_z)
hold on
plot(zeros(NumOfSample, 1), 'k')
ylabel('Absolute error')
ylim([-0.5 0.5])


figure()
semilogx(lambda3_star_set, 'color', [153, 0, 51]/255, 'LineWidth', 2)
hold on
semilogx(lambda2_star_set, 'k--', 'LineWidth', 1.2)
semilogx(lambda1_star_set, 'r--', 'LineWidth', 1.2)
semilogx(arith_mean_set, 'b-', 'LineWidth', 1.2)
semilogx(geo_mean_set, 'k-', 'LineWidth', 1.2)
semilogx(median_set, 'g-', 'LineWidth', 1.2)
ylim([0 2])
xlabel('number of samples')
ylabel('estimated scale')
legend('\lambda^{\ast}','\lambda_{x}^{\ast}','\lambda_{y}^{\ast}','arith.mean','geo.mean','median','Orientation','horizontal')

