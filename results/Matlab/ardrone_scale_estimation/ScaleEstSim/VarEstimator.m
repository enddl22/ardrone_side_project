Var = VarEstimator(datafile)
% Variance estimation of input data
% Input:
%       DATA: m-by-d input data
%       Var:  d-by-d variance matrix


data = importdata(datafile, ',', 1);

height = data.data(:, 14);

Var = std(height);