clear all;
close all;
%tf = importdata('tf.txt',',',1);
%fid = fopen('tf.txt');
fid = fopen('y-axis01_0.85.txt');
header = fscanf(fid,'%s',1);
% format=['%f,' '%f,' '%f,' '%7c' '%5c' '%f,'  '%f,' '%f,' '%f,' '%f,' '%f,' '%f,'];
format=['%f' '%f' '%f' '%s' '%s' '%f'  '%f' '%f' '%f' '%f' '%f' '%f'];
outp=textscan(fid,format,'delimiter', ',');
fclose(fid);

temp = strcat(outp{1,5});
n=numel(temp);
idx_w2imu=[];
idx_ptam_fixed2body=[];
idx_w2ptam_fixed=[];
for i=1:n
    if(strcmp(temp(i),'/imu'))
        idx_w2imu=[idx_w2imu;i];
    elseif(strcmp(temp(i),'/body'))
        idx_ptam_fixed2body=[idx_ptam_fixed2body;i];
    elseif(strcmp(temp(i),'/ptam_fixed'))
        idx_w2ptam_fixed=[idx_w2ptam_fixed;i];
    end       
end

w2imu_data=[];
w2ptam_fixed_data=[];
ptam_fixed2body_data=[];

for i=1:3
    w2imu_data(:,i) = outp{1,i}(idx_w2imu);
    w2ptam_fixed_data(:,i) = outp{1,i}(idx_w2ptam_fixed);
    ptam_fixed2body_data(:,i) = outp{1,i}(idx_ptam_fixed2body);
    
end

for i=6:12
    w2imu_data(:,i) = outp{1,i}(idx_w2imu);
    w2ptam_fixed_data(:,i) = outp{1,i}(idx_w2ptam_fixed);
    ptam_fixed2body_data(:,i) = outp{1,i}(idx_ptam_fixed2body);
end

time = struct('w2imu',w2imu_data(:,1),'w2ptam_fixed',w2ptam_fixed_data(:,1),...
              'ptam_fixed2body',ptam_fixed2body_data(:,1));
z = struct('imu',w2imu_data(:,8),'ptam_fixed',w2ptam_fixed_data(:,8),'body',-ptam_fixed2body_data(:,8));

z_intp_ptam_fixed=interp1(time.w2ptam_fixed,z.ptam_fixed,time.w2imu,'spline');
z_intp_body=interp1(time.ptam_fixed2body,z.body,time.w2imu,'spline');

y= z.imu-z_intp_ptam_fixed;
x= z_intp_body;

figure;
%plot(time.w2imu,y,'r');
plot(y,'r');
legend('Sonar measurement');
figure;
plot(x,'b');
%plot(time.w2imu,x,'b');
legend('PTAM measurement');

a=y(180:571);
b=ones(numel(x(180:571)),1);
c=x(180:571);

f = @(lamda)sum(    (a-(lamda(1)*b+lamda(2)*c)).^2          );
options = optimset('Display','Iter','MaxIter',2000,'MaxFunEvals',20000);
tic
[lamda,fval] = fminsearch(f,[1 1],options);
lamda
toc





%test = fscanf(fid,'%f,%f,%f,%s,%s,%f,%f,%f,%f,%f,%f,%f',[1 12])
%test1 = fscanf(fid,',%s,%s,',2)

%{
data=[];
for n=1:10
   data(n)=fscanf(fid,'%s');
end
%}
