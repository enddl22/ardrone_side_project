%% Machine Learning Online Class - Exercise 1: Linear Regression

%  Instructions
%  ------------
% 
%  This file contains code that helps you get started on the
%  linear exercise. You will need to complete the following functions 
%  in this exericse:
%
%     warmUpExercise.m
%     plotData.m
%     gradientDescent.m
%     computeCost.m
%     gradientDescentMulti.m
%     computeCostMulti.m
%     featureNormalize.m
%     normalEqn.m
%
%  For this exercise, you will not need to change any code in this file,
%  or any other files other than those mentioned above.
%
% x refers to the population size in 10,000s
% y refers to the profit in $10,000s
%

%% Initialization
clear ; close all; clc

%scale_esti
debug_scale_estimation;
%% ======================= Part 1: Plotting =======================
fprintf('Plotting Data ...\n')
%data = load('ex1data1.txt');
%X = data(:, 1); y = data(:, 2);
win_size=50;
% store the initial data
%x_init=x(601:800);
%y_init=y(601:800);
x_init=x(1:win_size*4);
y_init=y(1:win_size*4);


% specify the number of training samples
no_train_samples=round(4.8*length(y_init)/5);

X=double(x_init(1:no_train_samples));
y=y_init(1:no_train_samples);

X_test=x_init((no_train_samples+1):end);
y_test=y_init((no_train_samples+1):end);

m = length(y); % number of training examples

% Plot Data
% Note: You have to complete the code in plotData.m
plotData(X, y);

fprintf('Program paused. Press enter to continue.\n');
pause;


%% =================== Part 3: Gradient descent ===================
tic
fprintf('Running Gradient Descent ...\n')

X = [ones(m, 1), X(:,1)]; % Add a column of ones to x
theta = zeros(2, 1); % initialize fitting parameters

% Some gradient descent settings
iterations = 30000;
%alpha = 0.17;
alpha = 0.9;

% compute and display initial cost
computeCost(X, y, theta)

% run gradient descent
[theta J_his]= gradientDescent(X, y, theta, alpha, iterations);
toc
% print theta to screen
fprintf('Theta found by gradient descent: ');
fprintf('%f %f \n', theta(1), theta(2));

% Plot the linear fit
%figure(3)
hold on; % keep previous plot visible
plot(X(:,2), X*theta, '-')
legend('Training data', 'Linear regression')
hold off % don't overlay any more plots on this figure

%plot the history of the cost function J
figure(4)
plot([1:length(J_his)],J_his)
xlabel('Number of Iterations')
ylabel('Cost function J')
legend('History of the Cost function J')


% % Predict values for population sizes of 35,000 and 70,000
% predict1 = [1, 3.5] *theta;
% fprintf('For population = 35,000, we predict a profit of %f\n',...
%     predict1*10000);
% predict2 = [1, 7] * theta;
% fprintf('For population = 70,000, we predict a profit of %f\n',...
%     predict2*10000);
% 
% fprintf('Program paused. Press enter to continue.\n');
% pause;



% %% ============= Part 4: Visualizing J(theta_0, theta_1) =============
% fprintf('Visualizing J(theta_0, theta_1) ...\n')
% 
% % Grid over which we will calculate J
% theta0_vals = linspace(-10, 10, 100);
% theta1_vals = linspace(-1, 4, 100);
% 
% % initialize J_vals to a matrix of 0's
% J_vals = zeros(length(theta0_vals), length(theta1_vals));
% 
% % Fill out J_vals
% for i = 1:length(theta0_vals)
%     for j = 1:length(theta1_vals)
% 	  t = [theta0_vals(i); theta1_vals(j)];    
% 	  J_vals(i,j) = computeCost(X, y, t);
%     end
% end
% 
% 
% % Because of the way meshgrids work in the surf command, we need to 
% % transpose J_vals before calling surf, or else the axes will be flipped
% J_vals = J_vals';
% % Surface plot
% figure;
% surf(theta0_vals, theta1_vals, J_vals)
% xlabel('\theta_0'); ylabel('\theta_1');
% 
% % Contour plot
% figure;
% % Plot J_vals as 15 contours spaced logarithmically between 0.01 and 100
% contour(theta0_vals, theta1_vals, J_vals, logspace(-2, 3, 20))
% xlabel('\theta_0'); ylabel('\theta_1');
% hold on;
% plot(theta(1), theta(2), 'rx', 'MarkerSize', 10, 'LineWidth', 2);

%% Evaluate found \theta with testing data

X_test = [ones(size(X_test,1), 1), X_test(:,1)]; % Add a column of ones to X_test
plotData(X_test(:,2),y_test)
hold on
plot(X_test(:,2),X_test*theta,'-')
legend('Testing data','Fitting to testing data by trained Parameters')

%% plot all the data in a time horizonal axis
X_fitting=[ones(length(x_init),1) x_init];
figure
plot([1:length(x_init)],x_init,'b',[1:length(y_init)],y_init,'r',[1:length(X_fitting(:,2))],X_fitting*theta,'g' )
xlabel('time')
ylabel('z coordinate in PTAM and world')
legend('z in PTAM','z in world','z after fitting')


% ptam_y = ptam_fixed2body_data(:,7);

%ptam_y_test = [ones(size(ptam_y,1), 1), ptam_y]; % Add a column of ones to X_test

%figure
%plot([1:length(ptam_y)],ptam_y_test*theta);

figure 

plotData(x_init,y_init)
hold on
plot(x_init, lamda(1)+x_init*lamda(2))