data = importdata('./ardrone_sonar_vs_groundtruth.txt',',',1);

height_ground_truth = data.data(:,1);
height_sonar_measurements = data.data(:,2);

figure();
plot(height_ground_truth,'ro');
hold;
plot(height_sonar_measurements,'b*');
grid on;
set(gca,'fontsize',16);
xlabel('Trial', 'fontsize',16);
ylabel('Metre', 'fontsize',16);
h_legend=legend('Ground Truth','Sonar');
set(h_legend,'fontsize',16);

error = height_ground_truth - height_sonar_measurements;
fprintf('sigma = %f, mean error = %f\n',std(error),mean(error));




