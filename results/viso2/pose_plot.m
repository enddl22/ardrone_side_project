pose = importdata('viso_trj.txt',',',1);
time = pose.data(:,1)/1e9;

%x,y,z (camera coordinate) z=forward.
p = pose.data(:,1:3);

plot3(p(:,3),p(:,1),p(:,2),'ro');

xlim([-100 100])
ylim([-100 100])
zlim([0 100])

grid on;